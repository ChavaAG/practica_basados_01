################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../source/General_functions.c \
../source/RTC_and_EEPROM_Functions.c \
../source/SPI_functions.c \
../source/Task_functions.c \
../source/main.c \
../source/terminal.c 

OBJS += \
./source/General_functions.o \
./source/RTC_and_EEPROM_Functions.o \
./source/SPI_functions.o \
./source/Task_functions.o \
./source/main.o \
./source/terminal.o 

C_DEPS += \
./source/General_functions.d \
./source/RTC_and_EEPROM_Functions.d \
./source/SPI_functions.d \
./source/Task_functions.d \
./source/main.d \
./source/terminal.d 


# Each subdirectory must supply rules for building sources it contributes
source/%.o: ../source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -D"CPU_MK64FN1M0VDC12" -DFSL_RTOS_FREE_RTOS -DSDK_OS_FREE_RTOS -I../CMSIS -I../board -I../drivers -I../freertos/Source/include -I../freertos/Source/portable/GCC/ARM_CM4F -I../source -I../startup -I../utilities -std=c99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


