/*
 * Copyright (c) 2013 - 2016, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * This is template for main module created by New Kinetis SDK 2.x Project Wizard. Enjoy!
 **/

#include <string.h>
#include "Task_Functions.h"
#include "board.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "fsl_gpio.h"
/*#include "fsl_debug_console.h"*/

/* FreeRTOS kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "terminal.h"
#include "timers.h"
#include "semphr.h"
#include "RTC_and_EEPROM_Functions.h"
#include "event_groups.h"
#include "General_functions.h"

/* Task priorities. */
#define dateTime_task_PRIORITY (configMAX_PRIORITIES - 1)
#define menuPrincipal_task_PRIORITY (configMAX_PRIORITIES - 2)
#define menus_task_PRIORITY (configMAX_PRIORITIES - 3)
#define ECHO_BUFFER_LENGTH 8

EventGroupHandle_t xCreatedEventGroup;
//SemaphoreHandle_t xBinarySemaphore;

uint8_t g_txBuffer[ECHO_BUFFER_LENGTH] = {0};
uint8_t g_rxBuffer[ECHO_BUFFER_LENGTH] = {0};
uart_config_t uartConfig;
uart_transfer_t transfer;
uart_handle_t g_uartHandle;
uart_transfer_t sendXfer;
uart_transfer_t receiveXfer;
volatile bool rxBufferEmpty = true;
volatile bool txBufferFull = false;
volatile bool txOnGoing = false;
volatile bool rxOnGoing = false;
volatile uint8_t selector = 0;
volatile uint8_t cont = 0;

TaskHandle_t menuPrincipal_handle;
TaskHandle_t leerMemoria_handle;
TaskHandle_t escribirMemoria_handle;
TaskHandle_t establecerHora_handle;
TaskHandle_t establecerFecha_handle;
TaskHandle_t leerHora_handle;
TaskHandle_t leerFecha_handle;
TaskHandle_t dateTime_handle;

uint8_t menuString[] = "\033[2J\033[1;1H1) Leer Memoria I2C \r\n2) Escribir memoria I2C \r\n3) Establecer Hora"
		"\r\n4) Establecer Fecha\r\n5) Formato de Hora\r\n6) Leer Hora\r\n7) Leer Fecha\r\n"
		"8) Comunicacion con terminal 2\r\n9) Eco en LCD  ";

void PORTC_IRQHandler()
{
	switch(PORT_GetPinsInterruptFlags(PORTC))
	{
	case 0x01:
		GPIO_setFlagPortCX(BIT0, TRUE);
		PORT_ClearPinsInterruptFlags(PORTC, 0x1<<PB0);
		break;
	case 0x02:
		GPIO_setFlagPortCX(BIT1, TRUE);
		PORT_ClearPinsInterruptFlags(PORTC, 0x1<<PB1);
		break;
	case 0x04:
		GPIO_setFlagPortCX(BIT2, TRUE);
		PORT_ClearPinsInterruptFlags(PORTC, 0x1<<PB2);
		break;
	case 0x08:
		GPIO_setFlagPortCX(BIT3, TRUE);
		PORT_ClearPinsInterruptFlags(PORTC, 0x1<<PB3);
		break;
	case 0x10:
		GPIO_setFlagPortCX(BIT4, TRUE);
		PORT_ClearPinsInterruptFlags(PORTC, 0x1<<PB4);
		break;
	case 0x20:
		GPIO_setFlagPortCX(BIT5, TRUE);
		PORT_ClearPinsInterruptFlags(PORTC, 0x1<<PB5);
		break;
	}
}


/*!
 * @brief Task responsible for printing of "Hello world." message.
 */
static void menuPrincipal_task(void *pvParameters) {
	uint8_t flag;
	uint8_t dir[] = "0x0000";
	uint8_t escritura[50];
	uint8_t cantidad[] = "00";
	uint8_t z = 10;
	xQueue = xQueueCreate(2, sizeof(dir));
	terminalMenu();
	escribirMenu("\033[17;17H  ");
	for(;;)
	{
		//xSemaphoreTake(xBinarySemaphore, portMAX_DELAY);
		if(0 != uxQueueMessagesWaiting( xQueue ))
		{
			xQueueReceive(xQueue, &flag, portMAX_DELAY);
			cont = 0;
		}

		/* If RX is idle and g_rxBuffer is empty, start to read data to g_rxBuffer. */
		if ((!rxOnGoing) && rxBufferEmpty)
		{
			rxOnGoing = true;
			UART_TransferReceiveNonBlocking(UART0, &g_uartHandle, &receiveXfer, NULL);
			if(g_rxBuffer[0] > 48 && g_rxBuffer[0] < 58)
			{
				selector = g_rxBuffer[0];
			}
			if(1 == flag && 6 > cont)
			{
				dir[cont] = g_rxBuffer[0];
				cont++;
				selector = 49;
			}
			if(6 == flag && 2 > cont)
			{
				cantidad[cont] = g_rxBuffer[0];
				cont++;
				selector = 49;
			}
			/*
			 * Si la bandera de
			 */
			if(2 == flag && 6 > cont)
			{
				dir[cont] = g_rxBuffer[0];
				cont++;
				selector = 50;
			}
			/*
			 * Si la bandera de escritura esta activa, guardar todos los datos que se van introduciendo
			 *
			 */
			if(3 == flag)
			{
				escritura[cont] = g_rxBuffer[0];
				cont++;
				selector = 50;
			}

			if(4 == flag && 6 > cont)
			{
				dir[cont] = g_rxBuffer[0];
				cont++;
				selector = 51;
			}
			if(5 == flag && 6 > cont)
			{
				dir[cont] = g_rxBuffer[0];
				cont++;
				selector = 52;
			}
			/*
			 * si se presiona enter se entra a un switch case donde se chequea cual fue la opci�n
			 * que se escogio  y se activa la bandera correspondiente.
			 */
			if(0x1B == g_rxBuffer[0])
			{
				escribirMenu("\033[17;17H  ");
				selector = 0;
				flag = 0;
			}
			if( 0xD == g_rxBuffer[0])
			{
				if(0 != selector)
				{
					switch(selector)
					{
					case 49:
						if(1 == flag || 6 == flag)
						{
							xMessage* message;
							message = (xMessage*)pvPortMalloc(sizeof(xMessage));
							if(1 == flag)
							{
								escribirMenu("\033[2;19H  ");
								message->x= dir;
							}
							else if(6 == flag)
							{
								message->x= cantidad;
							}
							xQueueSend( xQueue, &(message), portMAX_DELAY );
						}
						break;
					case 50:
						//xEventGroupSetBits(xCreatedEventGroup, 0x2);
						if(2 == flag || 3==flag)
						{
							xMessage* message;
							message = (xMessage*)pvPortMalloc(sizeof(xMessage));
							if(flag == 2)
							{
								message->x= dir;
							}
							if(flag == 3)
							{
								message->x= escritura;
							}
							xQueueSend( xQueue, &(message), portMAX_DELAY );
						}
						break;
					case 51:
						if(4 == flag)
						{
							xMessage* message;
							message = (xMessage*)pvPortMalloc(sizeof(xMessage));
							message->x= dir;
							xQueueSend( xQueue, &(message), portMAX_DELAY );
						}
						break;
					case 52:
						if(5 == flag)
						{
							xMessage* message;
							message = (xMessage*)pvPortMalloc(sizeof(xMessage));
							message->x= dir;
							xQueueSend( xQueue, &(message), portMAX_DELAY );
						}
						break;
					case 53:
						break;
					case 54:
						break;
					case 55:
						break;
					}
				}
				else
				{
					escribirMenu("\033[17;17H  ");
				}
			}
		}
		/* If TX is idle and g_txBuffer is full, start to send data. */
		if ((!txOnGoing) && txBufferFull)
		{
			txOnGoing = true;
			UART_TransferSendNonBlocking(UART0, &g_uartHandle, &sendXfer);
		}
		/* If g_txBuffer is empty and g_rxBuffer is full, copy g_rxBuffer to g_txBuffer. */
		if ((!rxBufferEmpty) && (!txBufferFull))
		{
			memcpy(g_txBuffer, g_rxBuffer, ECHO_BUFFER_LENGTH);
			rxBufferEmpty = true;
			txBufferFull = true;
		}
	}

}

static void leerMemoria_task(void *pvParameters)
{
	for(;;)
	{

	}
}

static void escribirMemoria_task(void *pvParameters)
{
	for(;;)
	{

	}
}

static void establecerHora_task(void *pvParameters)
{
	for(;;)
	{

	}
}

static void establecerFecha_task(void *pvParameters)
{
	for(;;)
	{

	}
}

static void leerHora_task(void *pvParameters)
{
	for(;;)
	{

	}
}

static void leerFecha_task(void *pvParameters)
{
	for(;;)
	{

	}
}

static void dateTime_task(void *pvParameters)
{
	for(;;)
	{

	}
}

int main(void) {
	/* Init board hardware. */
	BOARD_InitPins();
	BOARD_BootClockRUN();
	BOARD_InitDebugConsole();

	/* Add your code here */
	CLOCK_EnableClock(kCLOCK_PortA);
	CLOCK_EnableClock(kCLOCK_PortC);
	CLOCK_EnableClock(kCLOCK_PortD);
	CLOCK_EnableClock(kCLOCK_PortE);

	//xBinarySemaphore = xSemaphoreCreateBinary();
	gpio_pin_config_t config_gpio =
	{ kGPIO_DigitalInput };

	port_pin_config_t config_port =
	{ kPORT_PullUp, kPORT_FastSlewRate, kPORT_PassiveFilterDisable,
			kPORT_OpenDrainDisable, kPORT_LowDriveStrength, kPORT_MuxAsGpio,
			kPORT_UnlockRegister, };

	//xQueue = xQueueCreate(2, sizeof(uint8_t[6]));

	PORT_SetPinConfig(PORTC, PUSHBUTTON5, &config_port);
	GPIO_PinInit(GPIOC, PUSHBUTTON5, &config_gpio);
	PORT_SetPinInterruptConfig(PORTC, PUSHBUTTON5, kPORT_InterruptFallingEdge);

	PORT_SetPinConfig(PORTC, PUSHBUTTON4, &config_port);
	GPIO_PinInit(GPIOC, PUSHBUTTON4, &config_gpio);
	PORT_SetPinInterruptConfig(PORTC, PUSHBUTTON4, kPORT_InterruptFallingEdge);

	PORT_SetPinConfig(PORTC, PUSHBUTTON3, &config_port);
	GPIO_PinInit(GPIOC, PUSHBUTTON3, &config_gpio);
	PORT_SetPinInterruptConfig(PORTC, PUSHBUTTON3, kPORT_InterruptFallingEdge);

	PORT_SetPinConfig(PORTC, PUSHBUTTON2, &config_port);
	GPIO_PinInit(GPIOC, PUSHBUTTON2, &config_gpio);
	PORT_SetPinInterruptConfig(PORTC, PUSHBUTTON2, kPORT_InterruptFallingEdge);

	PORT_SetPinConfig(PORTC, PUSHBUTTON1, &config_port);
	GPIO_PinInit(GPIOC, PUSHBUTTON1, &config_gpio);
	PORT_SetPinInterruptConfig(PORTC, PUSHBUTTON1, kPORT_InterruptFallingEdge);

	PORT_SetPinConfig(PORTC, PUSHBUTTON0, &config_port);
	GPIO_PinInit(GPIOC, PUSHBUTTON0, &config_gpio);
	PORT_SetPinInterruptConfig(PORTC, PUSHBUTTON0, kPORT_InterruptFallingEdge);

	NVIC_SetPriority(PORTC_IRQn, 7);
	NVIC_EnableIRQ(PORTC_IRQn);

	terminalInit();
	I2C_init();

	/* Create RTOS task */
	xTaskCreate(menuPrincipal_task, "MenuPrincipal_task", 3*configMINIMAL_STACK_SIZE, NULL, menuPrincipal_task_PRIORITY, &menuPrincipal_handle);
	xTaskCreate(leerMemoria_task, "LeerMemoria_task", 3*configMINIMAL_STACK_SIZE, NULL, menus_task_PRIORITY, &leerMemoria_handle);
	xTaskCreate(escribirMemoria_task, "EscribirMemoria_task", 3*configMINIMAL_STACK_SIZE, NULL, menus_task_PRIORITY, &escribirMemoria_handle);
	xTaskCreate(establecerHora_task, "EstablecerHora_task", 3*configMINIMAL_STACK_SIZE, NULL, menus_task_PRIORITY, &establecerHora_handle);
	xTaskCreate(establecerFecha_task, "EstablecerFecha_task", 3*configMINIMAL_STACK_SIZE, NULL, menus_task_PRIORITY, &establecerFecha_handle);
	//xTaskCreate(formatoHora_task, "FormatoHora_task", configMINIMAL_STACK_SIZE, NULL, menus_task_PRIORITY, NULL);
	xTaskCreate(leerHora_task, "LeerHora_task", 3*configMINIMAL_STACK_SIZE, NULL, menus_task_PRIORITY, &leerHora_handle);
	xTaskCreate(leerFecha_task, "LeerFecha_task", 3*configMINIMAL_STACK_SIZE, NULL, menus_task_PRIORITY, &leerFecha_handle);
	xTaskCreate(dateTime_task, "dateTime_task", 3*configMINIMAL_STACK_SIZE, NULL, dateTime_task_PRIORITY, &dateTime_handle);
	vTaskSuspend(leerMemoria_handle);
	vTaskSuspend(escribirMemoria_handle);
	vTaskSuspend(establecerHora_handle);
	vTaskSuspend(establecerFecha_handle);
	vTaskSuspend(leerHora_handle);
	vTaskSuspend(leerFecha_handle);

	vTaskStartScheduler();


	for(;;) { /* Infinite loop to avoid leaving the main function */
		__asm("NOP"); /* something to use as a breakpoint stop while looping */
	}
}



