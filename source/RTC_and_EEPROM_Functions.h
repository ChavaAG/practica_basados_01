/*
 * RTC_Functions.h
 *
 *  Created on: 17/03/2017
 *      Author: Salvador
 */

#ifndef SOURCE_RTC_AND_EEPROM_FUNCTIONS_H_
#define SOURCE_RTC_AND_EEPROM_FUNCTIONS_H_

#include <string.h>
#include "board.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "fsl_port.h"
#include "fsl_i2c.h"
#include "fsl_gpio.h"

/*Address value to write data to the memory*/
#define WRITE_SRAM 0xA0
/*Address value to read data from the memory*/
#define READ_SRAM 0XA1
/*Address value to read data from the RTC*/
#define READ_RTC 0xDF
/*Address value to write data to the RTC*/
#define WRITE_RTC 0xDE
/*This is a mask to get the high part from the address of the RAM*/
#define HIGH_RAM 0x7F00
/*This is a mask to get the low part from the address of the RAM*/
#define LOW_RAM 0x00FF
/*With this mask, we start the counter of the RTC*/
#define START_OSC 0x80
/*Address value of the register of the seconds*/
#define SECONDS 0x00
/*Address value of the register of the minutes*/
#define MINUTES 0X01
/*Address value of the register of the hours*/
#define HOURS 0x02
/*Address value of the register of the days*/
#define DAYS 0x04
/*Address value of the register of the months*/
#define MONTHS 0x05
/*Address value of the register of the years*/
#define YEARS 0x06
/*Default hours value of the RTC*/
#define DEFAULT_HOURS 0x12
/*Default minutes value of the RTC*/
#define DEFAULT_MINUTES 0x00
/*Default seconds value of the RTC*/
#define DEFAULT_SECONDS 0x00
/*Default days value of the RTC*/
#define DEFAULT_DAYS 0x01
/*Default months value of the RTC*/
#define DEFAULT_MONTHS 0x01
/*Default years value of the RTC*/
#define DEFAULT_YEARS 0x00

static uint8_t dataFromEEPROM;
static uint8_t seconds;
static uint8_t minutes;
static uint8_t hours;
static uint8_t day;
static uint8_t month;
static uint8_t year;

static i2c_master_transfer_t masterXfer;
static i2c_master_handle_t g_m_handle;
static uint8_t data_to_send;
volatile bool g_MasterCompletionFlag = false;

void I2C_init(void);

static void i2c_master_callback(I2C_Type *base, i2c_master_handle_t *handle, status_t status, void *userData);

/*Sequence to read a byte from the EEPROM*/
uint8_t EEPROM_getData(uint16_t Address);

/*Function to write more than a byte to the EEPROM*/
void EPROM_writeDataInMemory(uint32_t text[], uint8_t maxElement, uint16_t address);

/*Sequence to write a byte to the EEPROM*/
void EEPROM_writeData(uint16_t Address, uint8_t Byte);

/*Sequence to read from the RTC for the seconds*/
uint8_t RTC_getSeconds();

/*Sequence to write on the RTC for the seconds*/
void RTC_setSeconds(uint8_t Second);

/*Sequence to read from the RTC for the minutes*/
uint8_t RTC_getMinutes();

/*Sequence to write on the RTC for the minutes*/
void RTC_setMinutes(uint8_t Minute);

/*Sequence to read from the RTC for the hours*/
uint8_t RTC_getHours();

/*Sequence to write on the RTC for the hours*/
void RTC_setHours(uint8_t Hour);

/*Sequence to read from the RTC for the days*/
uint8_t RTC_getDays();

/*Sequence to write on the RTC for the days*/
void RTC_setDays(uint8_t Day);

/*Sequence to read from the RTC for the months*/
uint8_t RTC_getMonths();

/*Sequence to write on the RTC for the months*/
void RTC_setMonths(uint8_t Month);

/*Sequence to read from the RTC for the years*/
uint8_t RTC_getYears();

/*Sequence to write on the RTC for the years*/
void RTC_setYears(uint8_t Year);

/*Function to changed the days, months and years of the RTC*/
void RTC_writeDateInRTC(uint8_t text[]);

/*Function to changed hours,minutes and seconds of the RTC*/
void RTC_writeTimeInRTC(uint8_t text[]);

#endif /* SOURCE_RTC_AND_EEPROM_FUNCTIONS_H_ */

void GPIO_delay(uint32_t delay);
