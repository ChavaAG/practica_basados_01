/*
 * General functions.c
 *
 *  Created on: 22/03/2017
 *      Author: Salvador
 */

#include "General_functions.h"

static struct {
	uint8_t FlagPortC0 : 1;              /*Aumentar amplitude_Volume*/
	uint8_t FlagPortC1 : 1;              /*Aumentar frecuencia de muestreo*/
	uint8_t FlagPortC2 : 1;              /*Disminuir amplitude_Volume*/
	uint8_t FlagPortC3 : 1;              /*Disminuir frecuencia de muestreo*/
	uint8_t FlagPortC4 : 1;              /*Aumentar amplitude_Volume*/
	uint8_t FlagPortC5 : 1;              /*Aumentar frecuencia de muestreo*/
} GPIO_interruptFlags;

uint8_t GPIO_getFlagPortCX(uint8_t pin){
	switch(pin){
	case BIT0:
		return GPIO_interruptFlags.FlagPortC0;
		break;
	case BIT1:
		return GPIO_interruptFlags.FlagPortC1;
		break;
	case BIT2:
		return GPIO_interruptFlags.FlagPortC2;
		break;
	case BIT3:
		return GPIO_interruptFlags.FlagPortC3;
		break;
	case BIT4:
		return GPIO_interruptFlags.FlagPortC4;
		break;
	default:
		return GPIO_interruptFlags.FlagPortC5;
		break;
	}
}

void GPIO_setFlagPortCX(uint8_t pin, uint8_t state){
	switch(pin){
	case BIT0:
		if(state){
			GPIO_interruptFlags.FlagPortC0 = TRUE;
		}
		else{
			GPIO_interruptFlags.FlagPortC0 = FALSE;
		}
		break;
	case BIT1:
		if(state){
			GPIO_interruptFlags.FlagPortC1 = TRUE;
		}
		else{
			GPIO_interruptFlags.FlagPortC1 = FALSE;
		}
		break;
	case BIT2:
		if(state){
			GPIO_interruptFlags.FlagPortC2 = TRUE;
		}
		else{
			GPIO_interruptFlags.FlagPortC2 = FALSE;
		}
		break;
	case BIT3:
		if(state){
			GPIO_interruptFlags.FlagPortC3 = TRUE;
		}
		else{
			GPIO_interruptFlags.FlagPortC3 = FALSE;
		}
		break;
	case BIT4:
		if(state){
			GPIO_interruptFlags.FlagPortC4 = TRUE;
		}
		else{
			GPIO_interruptFlags.FlagPortC4 = FALSE;
		}
		break;
	default:
		if(state){
			GPIO_interruptFlags.FlagPortC5 = TRUE;
		}
		else{
			GPIO_interruptFlags.FlagPortC5 = FALSE;
		}
		break;
	}
}


