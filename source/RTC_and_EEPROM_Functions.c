/*
 * RTC_Functions.c
 *
 *  Created on: 17/03/2017
 *      Author: Salvador
 */

#include "RTC_and_EEPROM_Functions.h"

static void i2c_master_callback(I2C_Type *base, i2c_master_handle_t *handle, status_t status, void *
		userData)
{
	/* Signal transfer success when received success status.*/
	if (status == kStatus_Success)
	{
		g_MasterCompletionFlag = true;
	}
}

void I2C_init(void)
{
	CLOCK_EnableClock(kCLOCK_PortE);

	port_pin_config_t config =
	{
			kPORT_PullUp,
			kPORT_FastSlewRate,
			kPORT_PassiveFilterDisable,
			kPORT_OpenDrainEnable,
			kPORT_LowDriveStrength,
			kPORT_MuxAlt5,
			kPORT_UnlockRegister,
	};

	PORT_SetPinConfig(PORTE,24u,&config);
	PORT_SetPinConfig(PORTE,25u,&config);

	i2c_master_config_t config_I2C = {
			.enableMaster = true,
			.enableStopHold = false,
			.enableHighDrive = false,
			.baudRate_Bps = 100000,
			.glitchFilterWidth = 0
	};

	I2C_MasterInit(I2C0, &config_I2C, CLOCK_GetFreq(I2C0_CLK_SRC));
	I2C_MasterTransferCreateHandle(I2C0, &g_m_handle,i2c_master_callback, NULL);

	RTC_setHours(DEFAULT_HOURS);
	RTC_setMinutes(DEFAULT_MINUTES);
	RTC_setDays(DEFAULT_DAYS);
	RTC_setMonths(DEFAULT_MONTHS);
	RTC_setYears(DEFAULT_YEARS);
	RTC_setSeconds(DEFAULT_SECONDS);

}

/*Sequence to read a byte from the EEPROM*/
uint8_t EEPROM_getData(uint16_t Address){
	GPIO_delay(10000);

	masterXfer.slaveAddress = WRITE_SRAM>>1;
	masterXfer.direction = kI2C_Read;
	masterXfer.subaddress = Address;
	masterXfer.subaddressSize = 2;
	masterXfer.data = &dataFromEEPROM;
	masterXfer.dataSize = 1;
	masterXfer.flags = kI2C_TransferDefaultFlag;

	I2C_MasterTransferNonBlocking(I2C0, &g_m_handle, &masterXfer);
	while (!g_MasterCompletionFlag);
	g_MasterCompletionFlag = false;

	GPIO_delay(10000);

	return dataFromEEPROM;
}

/*Function to write more than a byte to the EEPROM*/
void EPROM_writeDataInMemory(uint32_t text[], uint8_t maxElement, uint16_t address){
	uint8_t i;
	for(i=0; i < maxElement; i++){
		EEPROM_writeData(address, text[i]);
		address = address + 0x8;
	}
}

/*Sequence to write a byte to the EEPROM*/
void EEPROM_writeData(uint16_t Address, uint8_t Byte){
	GPIO_delay(100000);

	masterXfer.slaveAddress = WRITE_SRAM>>1;
	masterXfer.direction = kI2C_Write;
	masterXfer.subaddress = Address;
	masterXfer.subaddressSize = 2;
	masterXfer.data = &Byte;
	masterXfer.dataSize = 1;
	masterXfer.flags = kI2C_TransferDefaultFlag;

	I2C_MasterTransferNonBlocking(I2C0, &g_m_handle, &masterXfer);
	while (!g_MasterCompletionFlag);
	g_MasterCompletionFlag = false;

	GPIO_delay(100000);
}

/*Sequence to read from the RTC for the seconds*/
uint8_t RTC_getSeconds(){
	GPIO_delay(10000);

	masterXfer.slaveAddress = WRITE_RTC>>1;
	masterXfer.direction = kI2C_Read;
	masterXfer.subaddress = SECONDS;
	masterXfer.subaddressSize = 1;
	masterXfer.data = &seconds;
	masterXfer.dataSize = 1;
	masterXfer.flags = kI2C_TransferDefaultFlag;

	I2C_MasterTransferNonBlocking(I2C0, &g_m_handle, &masterXfer);
	while (!g_MasterCompletionFlag);
	g_MasterCompletionFlag = false;

	GPIO_delay(10000);

	return seconds;
}

/*Sequence to write on the RTC for the seconds*/
void RTC_setSeconds(uint8_t Second){
	GPIO_delay(10000);

	data_to_send = START_OSC|Second;
	masterXfer.slaveAddress = WRITE_RTC>>1;
	masterXfer.direction = kI2C_Write;
	masterXfer.subaddress = SECONDS;
	masterXfer.subaddressSize = 1;
	masterXfer.data = &data_to_send;
	masterXfer.dataSize = 1;
	masterXfer.flags = kI2C_TransferDefaultFlag;

	I2C_MasterTransferNonBlocking(I2C0, &g_m_handle, &masterXfer);
	while (!g_MasterCompletionFlag);
	g_MasterCompletionFlag = false;

	GPIO_delay(10000);
}

/*Sequence to read from the RTC for the minutes*/
uint8_t RTC_getMinutes(){
	GPIO_delay(10000);

	masterXfer.slaveAddress = WRITE_RTC>>1;
	masterXfer.direction = kI2C_Read;
	masterXfer.subaddress = MINUTES;
	masterXfer.subaddressSize = 1;
	masterXfer.data = &minutes;
	masterXfer.dataSize = 1;
	masterXfer.flags = kI2C_TransferDefaultFlag;

	I2C_MasterTransferNonBlocking(I2C0, &g_m_handle, &masterXfer);
	while (!g_MasterCompletionFlag);
	g_MasterCompletionFlag = false;

	GPIO_delay(10000);

	return minutes;
}

/*Sequence to write on the RTC for the minutes*/
void RTC_setMinutes(uint8_t Minute){
	GPIO_delay(10000);

	masterXfer.slaveAddress = WRITE_RTC>>1;
	masterXfer.direction = kI2C_Write;
	masterXfer.subaddress = MINUTES;
	masterXfer.subaddressSize = 1;
	masterXfer.data = &Minute;
	masterXfer.dataSize = 1;
	masterXfer.flags = kI2C_TransferDefaultFlag;

	I2C_MasterTransferNonBlocking(I2C0, &g_m_handle, &masterXfer);
	while (!g_MasterCompletionFlag);
	g_MasterCompletionFlag = false;

	GPIO_delay(10000);
}

/*Sequence to read from the RTC for the hours*/
uint8_t RTC_getHours(){
	GPIO_delay(10000);

	masterXfer.slaveAddress = WRITE_RTC>>1;
	masterXfer.direction = kI2C_Read;
	masterXfer.subaddress = HOURS;
	masterXfer.subaddressSize = 1;
	masterXfer.data = &hours;
	masterXfer.dataSize = 1;
	masterXfer.flags = kI2C_TransferDefaultFlag;

	I2C_MasterTransferNonBlocking(I2C0, &g_m_handle, &masterXfer);
	while (!g_MasterCompletionFlag);
	g_MasterCompletionFlag = false;

	GPIO_delay(10000);

	return hours;
}

/*Sequence to write on the RTC for the hours*/
void RTC_setHours(uint8_t Hour){
	GPIO_delay(10000);

	masterXfer.slaveAddress = WRITE_RTC>>1;
	masterXfer.direction = kI2C_Write;
	masterXfer.subaddress = HOURS;
	masterXfer.subaddressSize = 1;
	masterXfer.data = &Hour;
	masterXfer.dataSize = 1;
	masterXfer.flags = kI2C_TransferDefaultFlag;

	I2C_MasterTransferNonBlocking(I2C0, &g_m_handle, &masterXfer);
	while (!g_MasterCompletionFlag);
	g_MasterCompletionFlag = false;

	GPIO_delay(10000);
}

/*Sequence to read from the RTC for the days*/
uint8_t RTC_getDays(){
	GPIO_delay(10000);

	masterXfer.slaveAddress = WRITE_RTC>>1;
	masterXfer.direction = kI2C_Read;
	masterXfer.subaddress = DAYS;
	masterXfer.subaddressSize = 1;
	masterXfer.data = &day;
	masterXfer.dataSize = 1;
	masterXfer.flags = kI2C_TransferDefaultFlag;

	I2C_MasterTransferNonBlocking(I2C0, &g_m_handle, &masterXfer);
	while (!g_MasterCompletionFlag);
	g_MasterCompletionFlag = false;

	GPIO_delay(10000);

	return day;
}

/*Sequence to write on the RTC for the days*/
void RTC_setDays(uint8_t Day){
	GPIO_delay(10000);

	masterXfer.slaveAddress = WRITE_RTC>>1;
	masterXfer.direction = kI2C_Write;
	masterXfer.subaddress = DAYS;
	masterXfer.subaddressSize = 1;
	masterXfer.data = &Day;
	masterXfer.dataSize = 1;
	masterXfer.flags = kI2C_TransferDefaultFlag;

	I2C_MasterTransferNonBlocking(I2C0, &g_m_handle, &masterXfer);
	while (!g_MasterCompletionFlag);
	g_MasterCompletionFlag = false;

	GPIO_delay(10000);
}

/*Sequence to read from the RTC for the months*/
uint8_t RTC_getMonths(){
	GPIO_delay(10000);

	masterXfer.slaveAddress = WRITE_RTC>>1;
	masterXfer.direction = kI2C_Read;
	masterXfer.subaddress = MONTHS;
	masterXfer.subaddressSize = 1;
	masterXfer.data = &month;
	masterXfer.dataSize = 1;
	masterXfer.flags = kI2C_TransferDefaultFlag;

	I2C_MasterTransferNonBlocking(I2C0, &g_m_handle, &masterXfer);
	while (!g_MasterCompletionFlag);
	g_MasterCompletionFlag = false;

	GPIO_delay(10000);

	return month;
}

/*Sequence to write on the RTC for the months*/
void RTC_setMonths(uint8_t Month){
	GPIO_delay(10000);

	masterXfer.slaveAddress = WRITE_RTC>>1;
	masterXfer.direction = kI2C_Write;
	masterXfer.subaddress = MONTHS;
	masterXfer.subaddressSize = 1;
	masterXfer.data = &Month;
	masterXfer.dataSize = 1;
	masterXfer.flags = kI2C_TransferDefaultFlag;

	I2C_MasterTransferNonBlocking(I2C0, &g_m_handle, &masterXfer);
	while (!g_MasterCompletionFlag);
	g_MasterCompletionFlag = false;

	GPIO_delay(10000);
}

/*Sequence to read from the RTC for the years*/
uint8_t RTC_getYears(){
	GPIO_delay(10000);

	masterXfer.slaveAddress = WRITE_RTC>>1;
	masterXfer.direction = kI2C_Read;
	masterXfer.subaddress = YEARS;
	masterXfer.subaddressSize = 1;
	masterXfer.data = &year;
	masterXfer.dataSize = 1;
	masterXfer.flags = kI2C_TransferDefaultFlag;

	I2C_MasterTransferNonBlocking(I2C0, &g_m_handle, &masterXfer);
	while (!g_MasterCompletionFlag);
	g_MasterCompletionFlag = false;

	GPIO_delay(10000);

	return year;
}

/*Sequence to write on the RTC for the years*/
void RTC_setYears(uint8_t Year){
	GPIO_delay(10000);

	masterXfer.slaveAddress = WRITE_RTC>>1;
	masterXfer.direction = kI2C_Write;
	masterXfer.subaddress = YEARS;
	masterXfer.subaddressSize = 1;
	masterXfer.data = &Year;
	masterXfer.dataSize = 1;
	masterXfer.flags = kI2C_TransferDefaultFlag;

	I2C_MasterTransferNonBlocking(I2C0, &g_m_handle, &masterXfer);
	while (!g_MasterCompletionFlag);
	g_MasterCompletionFlag = false;

	GPIO_delay(10000);
}

/*Function to changed hours,minutes and seconds of the RTC*/
void RTC_writeTimeInRTC(uint8_t text[]){
	RTC_setHours((text[0]<<4)|text[1]);
	RTC_setMinutes((text[3]<<4)|text[4]);
	RTC_setSeconds((text[6]<<4)|text[7]);
}

/*Function to changed the days, months and years of the RTC*/
void RTC_writeDateInRTC(uint8_t text[]){
	RTC_setDays((text[0]<<4)|text[1]);
	RTC_setMonths((text[3]<<4)|text[4]);
	RTC_setYears((text[6]<<4)|text[7]);
}

void GPIO_delay(uint32_t delay){
	uint32_t counter;
	for(counter=delay;counter>0;counter--){
	}
}

