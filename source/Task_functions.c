/*
 * Task_functions.c
 *
 *  Created on: 01/04/2017
 *      Author: Salvador
 */

#include "Task_Functions.h"

void leerMemoria()
{
	uint8_t longitudFlag = 0;
	uint8_t *dir;
	uint16_t z = 0;
	uint8_t quantity = 0;
	uint8_t counter = 0;
	xMessage* message;

	message = (xMessage*)pvPortMalloc(sizeof(xMessage));
	escribirMenu("\033[2J\033[1;1HDireccion de lectura:  \r\nLongitud en bytes: \r\nContenido: ");
	escribirMenu("\033[1;22H  ");
	longitudFlag = 1;
	xQueueSend( xQueue, &longitudFlag, portMAX_DELAY );
	for (;;) {
		if(1 == longitudFlag)
		{
			if(0 != uxQueueMessagesWaiting( xQueue ))
			{
				xQueueReceive(xQueue, &(message), portMAX_DELAY);
				dir = message->x;
				vPortFree(message);
				for(uint8_t i = 2; i<6; i++)
				{
					if((47<dir[i]) && (58>dir[i]))
						z |= (dir[i] - 48) << (4*(6-(i+1)));
					else
						z |= (dir[i] - 55) << (4*(6-(i+1)));
				}
				longitudFlag = 6;
				xQueueSend( xQueue, &longitudFlag, portMAX_DELAY );
				//vTaskResume(menuPrincipal_handle);
			}
		}
		if(6 == longitudFlag)
		{
			/*
			 * Se recibe el valor de la direcci�n
			 */
			if(0 != uxQueueMessagesWaiting( xQueue ))
			{
				xQueueReceive(xQueue, &(message), portMAX_DELAY);
				dir = message->x;
				vPortFree(message);
				for(uint8_t i = 0; i<2; i++)
				{
					if((47<dir[i]) && (58>dir[i]))
						quantity |= (dir[i] - 48) << (4*(2-(i+1)));
					else
						quantity |= (dir[i] - 55) << (4*(2-(i+1)));
				}
				uint8_t text[quantity+1];
				escribirMenu("\033[4;1H  ");
				for(counter = 0; counter<quantity; counter++)
				{
					text[counter] = EEPROM_getData(z);
					z+=0x08;
				}
				text[quantity] = ' ';
				text[quantity+1] = '\0';
				escribirMenu(text);
				z = 0;
				quantity = 0;
				longitudFlag = 0;
				//vTaskResume(menuPrincipal_handle);
			}
		}
		escribirMenu("\033[2J\033[1;1HDireccion de lectura:  \r\nLongitud en bytes: \r\nContenido: ");
		escribirMenu("\033[1;22H  ");
		longitudFlag = 1;
		xQueueSend( xQueue, &longitudFlag, portMAX_DELAY );
		//vTaskResume(menuPrincipal_handle);
	}
}

void escribirMemoria()
{
	uint8_t longitudFlag = 0;
	uint8_t *dir;
	uint8_t z = 0;
	uint8_t cont = 0;
	xMessage* message;
	message = (xMessage*)pvPortMalloc(sizeof(xMessage));
	escribirMenu("\033[2J\033[1;1HDireccion de escritura:  \r\nTexto a guardar:  ");
	escribirMenu("\033[1;25H  ");
	longitudFlag = 2;
	xQueueSend( xQueue, &longitudFlag, portMAX_DELAY );
	//vTaskResume(menuPrincipal_handle);
	for (;;) {
		/*
		 * condicionales para ver si esta activa la bandera que permite ingresar direcci�n a la
		 * memoria o para ver si esta activa la bandera para esccribir en la memoria.
		 */
		if(2 == longitudFlag)
		{
			if(0 != uxQueueMessagesWaiting( xQueue ))
			{
				/*Se recibe el valor de la direcci�n*/
				xQueueReceive(xQueue, &(message), portMAX_DELAY);
				dir = message->x;
				vPortFree(message);
				/* Se convierten los valores del arreglo a hexa.*/
				for(uint8_t i = 2; i<6; i++)
				{
					if((47<dir[i]) && (58>dir[i]))
						z |= (dir[i] - 48) << (4*(6-(i+1)));
					else
						z |= (dir[i] - 55) << (4*(6-(i+1)));
				}
				longitudFlag = 3;
				escribirMenu("\033[2;20H  ");
				xQueueSend( xQueue, &longitudFlag, portMAX_DELAY );
				//vTaskResume(menuPrincipal_handle);
			}
		}
		if(3 == longitudFlag)
		{
			if(0 != uxQueueMessagesWaiting( xQueue ))
			{
				xQueueReceive(xQueue, &(message), portMAX_DELAY);
				dir = message->x;
				vPortFree(message);
				while((dir[cont]) != '0')
				{
					EEPROM_writeData(z, dir[cont]);
					z+=8;
					cont++;
				}
				escribirMenu("\033[7;18HSu texto ha sido guardado...");
				z = 0;
				cont = 0;
				longitudFlag = 0;
				//vTaskResume(menuPrincipal_handle);
			}
		}
		escribirMenu("\033[2J\033[1;1HDireccion de escritura:  \r\nTexto a guardar: ");
		escribirMenu("\033[1;23H  ");
		longitudFlag = 2;
		xQueueSend( xQueue, &longitudFlag, portMAX_DELAY );
		//vTaskResume(menuPrincipal_handle);
	}
}

void establecerHora() {
	uint8_t horaFlag = 0;
	xMessage* message;
	uint8_t *dir;
	message = (xMessage*)pvPortMalloc(sizeof(xMessage));
	escribirMenu("\033[2J\033[1;1HEscribir hora en hh/mm/ss:  ");
	escribirMenu("\033[1;27H  ");
	horaFlag = 4;
	xQueueSend( xQueue, &horaFlag, portMAX_DELAY );
	//vTaskResume(menuPrincipal_handle);
	for (;;) {
		if( 4 == horaFlag )
		{
			if(0 != uxQueueMessagesWaiting( xQueue ))
			{
				xQueueReceive(xQueue, &(message), portMAX_DELAY);
				dir = message->x;
				vPortFree(message);
				RTC_setHours(((dir[0]-48)<<4)|(dir[1]-48));
				RTC_setMinutes(((dir[2]-48)<<4)|(dir[3]-48));
				RTC_setSeconds(((dir[4]-48)<<4)|(dir[5]-48));
				escribirMenu("\033[2;18HLa hora ha sido cambiada... ");
				horaFlag = 0;
				//vTaskResume(menuPrincipal_handle);
			}
		}
		escribirMenu("\033[2J\033[1;1HEscribir hora en hh/mm/ss:  ");
		escribirMenu("\033[1;27H  ");
		horaFlag = 4;
		xQueueSend( xQueue, &horaFlag, portMAX_DELAY );
		//vTaskResume(menuPrincipal_handle);
	}
}

void establecerFecha() {
	uint8_t dateFlag = 0;
	xMessage* message;
	uint8_t *dir;
	message = (xMessage*)pvPortMalloc(sizeof(xMessage));
	escribirMenu("\033[2J\033[1;1HEscribir fecha en dd/mm/aa:  ");
	escribirMenu("\033[1;27H  ");
	dateFlag = 5;
	xQueueSend( xQueue, &dateFlag, portMAX_DELAY );
	//vTaskResume(menuPrincipal_handle);
	for (;;) {
		if( 5 == dateFlag )
		{
			if(0 != uxQueueMessagesWaiting( xQueue ))
			{
				xQueueReceive(xQueue, &(message), portMAX_DELAY);
				dir = message->x;
				vPortFree(message);
				RTC_setDays(((dir[0]-48)<<4)|(dir[1]-48));
				RTC_setMonths(((dir[2]-48)<<4)|(dir[3]-48));
				RTC_setYears(((dir[4]-48)<<4)|(dir[5]-48));
				escribirMenu("\033[2;18HLa fecha ha sido cambiada... ");
				dateFlag = 0;
				//vTaskResume(menuPrincipal_handle);
			}
		}
		escribirMenu("\033[2J\033[1;1HEscribir fecha en dd/mm/aa:  ");
		escribirMenu("\033[1;27H  ");
		dateFlag = 5;
		xQueueSend( xQueue, &dateFlag, portMAX_DELAY );
		//vTaskResume(menuPrincipal_handle);
	}
}

void leerHora() {
	uint8_t seconds = 0;
	uint8_t minutes = 0;
	uint8_t hours = 0;
	uint8_t hora [9];
	for (;;) {
		seconds = RTC_getSeconds();
		minutes = RTC_getMinutes();
		hours = RTC_getHours();
		escribirMenu("\033[2J\033[1;1HLa hora actual es:");
		escribirMenu("\033[3;1H  ");
		hora[0] = ((hours & 0xF0)>>4)+48;
		hora[1] = (hours & 0x0F)+48;
		hora[2] = ':';
		hora[3] = ((minutes & 0xF0)>>4)+48;
		hora[4] = (minutes & 0x0F)+48;
		hora[5] = ':';
		hora[6] = ((seconds & 0x70)>>4)+48;
		hora[7] = (seconds & 0x0F)+48;
		hora[9] = '\0';
		escribirMenu(hora);
		//vTaskResume(menuPrincipal_handle);
	}
}

void leerFecha() {
	uint8_t day = 0;
	uint8_t month = 0;
	uint8_t year = 0;
	uint8_t dia [9];
	for (;;) {
		day = RTC_getDays();
		month = RTC_getMonths();
		year = RTC_getYears();
		//Se tiene que convertir a hexa?
		escribirMenu("\033[2J\033[1;1HLa fecha actual es: ");
		escribirMenu("\033[3;1H  ");
		dia[0] = ((day & 0xF0)>>4)+48;
		dia[1] = (day & 0x0F)+48;
		dia[2] = '/';
		dia[3] = ((month & 0xF0)>>4)+48;
		dia[4] = (month & 0x0F)+48;
		dia[5] = '/';
		dia[6] = ((year & 0xF0)>>4)+48;
		dia[7] = (year & 0x0F)+48;
		dia[9] = '\0';
		escribirMenu(dia);
		//vTaskResume(menuPrincipal_handle);
	}
}

void dateTime() {
	uint8_t day = 0x0;
	uint8_t month = 0x0;
	uint8_t year = 0x0;
	uint8_t seconds = 0x0;
	uint8_t minutes = 0x0;
	uint8_t hours = 0x0;
	uint8_t dayCounter = 0x0;
	uint8_t monthCounter = 0x0;
	uint8_t yearCounter = 0x0;
	uint8_t secondsCounter = 0x0;
	uint8_t minutesCounter = 0x0;
	uint8_t hoursCounter = 0x0;
	uint8_t dia [9];
	uint8_t hora [9];
	uint8_t counter = 0;
	for (;;) {
		day = RTC_getDays();
		month = RTC_getMonths();
		year = RTC_getYears();
		seconds = RTC_getSeconds();
		minutes = RTC_getMinutes();
		hours = RTC_getHours();
		if(TRUE == GPIO_getFlagPortCX(BIT0)){
			dayCounter++;
			if(dayCounter == 0x0A){
				dayCounter = 0x10;
			}
			if(dayCounter == 0x1A){
				dayCounter = 0x20;
			}
			if(dayCounter == 0x2A){
				dayCounter = 0x30;
			}
			if(dayCounter == 0x31){
				dayCounter = 0x00;
				monthCounter++;
				month = monthCounter;
			}
			day = dayCounter;
			GPIO_setFlagPortCX(BIT0, FALSE);
		}
		if(TRUE == GPIO_getFlagPortCX(BIT1)){
			monthCounter++;
			if(monthCounter == 0x0A){
				monthCounter = 0x10;
			}
			if(monthCounter == 0x13){
				monthCounter = 0x00;
				yearCounter++;
				year = yearCounter;
			}
			month = monthCounter;
			GPIO_setFlagPortCX(BIT1, FALSE);
		}
		if(TRUE == GPIO_getFlagPortCX(BIT2)){
			yearCounter++;
			if((yearCounter&0x0F)== 0x0A)
			{
				yearCounter = (yearCounter & 0xF0) + 0x10;
			}
			if((yearCounter&0xF0)== 0xA0)
			{
				yearCounter = 0x00;
			}
			year = yearCounter;
			GPIO_setFlagPortCX(BIT2, FALSE);
		}
		if(TRUE == GPIO_getFlagPortCX(BIT3)){
			hoursCounter++;
			hours = hoursCounter;
			GPIO_setFlagPortCX(BIT3, FALSE);
		}
		if(TRUE == GPIO_getFlagPortCX(BIT4)){
			minutesCounter++;
			if((minutesCounter&0x0F) == 0x0A){
				minutesCounter = (minutesCounter & 0xF0) + 0x10;
			}
			if((minutesCounter&0xF0) == 0x60){
				minutesCounter = 0x00;
				hoursCounter++;
				hours = hoursCounter;
			}
			minutes = minutesCounter;
			GPIO_setFlagPortCX(BIT4, FALSE);
		}
		if(TRUE == GPIO_getFlagPortCX(BIT5)){
			secondsCounter++;
			if(secondsCounter == 0x0A){
				secondsCounter = 0x10;
			}
			if(secondsCounter == 0x6A){
				secondsCounter = 0x00;
				minutesCounter++;
				minutes = minutesCounter;
			}
			seconds = secondsCounter;
			GPIO_setFlagPortCX(BIT5, FALSE);
		}
		RTC_setYears(year);
		RTC_setMonths(month);
		RTC_setDays(day);
		RTC_setHours(hours);
		RTC_setMinutes(minutes);
		RTC_setSeconds(seconds);
		escribirMenu("\033[11;1HLa fecha actual es:  ");
		escribirMenu("\033[12;1H  ");
		dia[0] = ((day & 0x30)>>4)+48;
		dia[1] = (day & 0x0F)+48;
		dia[2] = '/';
		dia[3] = ((month & 0x00)>>4)+48;
		dia[4] = (month & 0x0F)+48;
		dia[5] = '/';
		dia[6] = ((year & 0xF0)>>4)+48;
		dia[7] = (year & 0x0F)+48;
		dia[9] = '\0';
		escribirMenu(dia);
		escribirMenu("\033[14;1HLa hora actual es:  ");
		escribirMenu("\033[15;1H  ");
		hora[0] = ((hours & 0x30)>>4)+48;
		hora[1] = (hours & 0x0F)+48;
		hora[2] = ':';
		hora[3] = ((minutes & 0x70)>>4)+48;
		hora[4] = (minutes & 0x0F)+48;
		hora[5] = ':';
		hora[6] = ((seconds & 0x70)>>4)+48;
		hora[7] = (seconds & 0x0F)+48;
		hora[9] = '\0';
		escribirMenu(hora);
		escribirMenu("\033[17;17H  ");
		vTaskDelay(ONE_SECOND);
	}
}


