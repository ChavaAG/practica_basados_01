/*
 * Task_Functions.h
 *
 *  Created on: 12/04/2017
 *      Author: Salvador
 */

#ifndef SOURCE_TASK_FUNCTIONS_H_
#define SOURCE_TASK_FUNCTIONS_H_

#include <string.h>
#include "FreeRTOS.h"
#include "queue.h"
#include "RTC_and_EEPROM_Functions.h"
#include "General_functions.h"

#define ONE_SECOND 1000
#define PUSHBUTTON0 0x00
#define PUSHBUTTON1 0x01
#define PUSHBUTTON2 0x02
#define PUSHBUTTON4 0x04
#define PUSHBUTTON3 0x03
#define PUSHBUTTON5 0x05
#define PB0 0u
#define PB1 1u
#define PB2 2u
#define PB3 3u
#define PB4 4u
#define PB5 5u

QueueHandle_t xQueue;

typedef struct
{
	uint8_t *x;
}xMessage;

static void leerMemoria();
static void escribirMemoria();
static void establecerHora();
static void establecerFecha();
static void leerHora();
static void leerFecha();
static void dateTime();


#endif /* SOURCE_TASK_FUNCTIONS_H_ */
