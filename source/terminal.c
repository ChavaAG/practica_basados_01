/*
 * terminal.c
 *
 *  Created on: 17/03/2017
 *      Author: Alberto
 */

#include "terminal.h"

#define ECHO_BUFFER_LENGTH 8

uart_config_t uartConfig;
uart_transfer_t transfer;
uart_handle_t g_uartHandle;
uart_transfer_t sendXfer;
uart_transfer_t receiveXfer;

uint8_t menuString[] = "\033[2J\033[1;1H1) Leer Memoria I2C \r\n2) Escribir memoria I2C \r\n3) Establecer Hora"
		"\r\n4) Establecer Fecha\r\n5) Formato de Hora\r\n6) Leer Hora\r\n7)Leer Fecha\r\n"
		"8) Comunicacion con terminal 2\r\n9) Eco en LCD";

uint8_t dir[] = "0x0000";

volatile bool rxBufferEmpty = true;
volatile bool txBufferFull = false;
volatile bool txOnGoing = false;
volatile bool rxOnGoing = false;
volatile uint8_t selector = 0;
volatile bool direccion = false;
volatile bool longitudB = false;
volatile uint8_t cont = 0;

uint8_t g_txBuffer[ECHO_BUFFER_LENGTH] = {0};
uint8_t g_rxBuffer[ECHO_BUFFER_LENGTH] = {0};
bool leerMemoriaFlag = false;
bool escribirMemoriaFlag = false;
bool establecerHoraFlag = false;
bool establecerFechaFlag = false;
bool formatoHoraFlag = false;


void UART_UserCallback(UART_Type *base, uart_handle_t *handle, status_t status, void *userData);

void UART_UserCallback(UART_Type *base, uart_handle_t *handle, status_t status, void *userData)
{
	userData = userData;
	int data;

	if (kStatus_UART_TxIdle == status)
	{
		txBufferFull = false;
		txOnGoing = false;
	}

	if (kStatus_UART_RxIdle == status)
	{
		rxBufferEmpty = false;
		rxOnGoing = false;
	}
}
/*
void UART0_RX_TX_IRQHandler(void)
{
	uint8_t data;

	if ((kUART_RxDataRegFullFlag | kUART_RxOverrunFlag) & UART_GetStatusFlags(UART0))
	{
		data = UART_ReadByte(UART0);


	}
}*/

void terminalInit()
{
	UART_GetDefaultConfig(&uartConfig);
	uartConfig.baudRate_Bps = BOARD_DEBUG_UART_BAUDRATE;
	uartConfig.enableTx = true;
	uartConfig.enableRx = true;
	UART_Init(UART0, &uartConfig, CLOCK_GetFreq(UART0_CLK_SRC));
	UART_TransferCreateHandle(UART0, &g_uartHandle, UART_UserCallback, NULL);
	UART_EnableInterrupts(UART0, kUART_RxDataRegFullInterruptEnable | kUART_RxOverrunInterruptEnable);
	EnableIRQ(UART0_RX_TX_IRQn);


}

void terminalMenu()
{
	/* Send g_tipString out. */
	transfer.data = menuString;
	transfer.dataSize = sizeof(menuString) - 1;
	txOnGoing = true;
	UART_TransferSendNonBlocking(UART0, &g_uartHandle, &transfer);
	while (txOnGoing)
	{
	}
	sendXfer.data = g_txBuffer;
	sendXfer.dataSize = 4;
	receiveXfer.data = g_rxBuffer;
	receiveXfer.dataSize = 1;

	UART_EnableInterrupts(UART0, kUART_RxDataRegFullInterruptEnable | kUART_RxOverrunInterruptEnable);
	EnableIRQ(UART0_RX_TX_IRQn);

}
void terminalCambioMenu()
{
	uint8_t cont = 0;
	for(;;)
	{
		/* If RX is idle and g_rxBuffer is empty, start to read data to g_rxBuffer. */
		if ((!rxOnGoing) && rxBufferEmpty)
		{
			rxOnGoing = true;
			UART_TransferReceiveNonBlocking(UART0, &g_uartHandle, &receiveXfer, NULL);
			/*
			 * Condicional para ver cual bandera del menu esta activa.
			 */
			if(true == leerMemoriaFlag || true == escribirMemoriaFlag || true == establecerHoraFlag
			   || true == establecerFechaFlag || true == formatoHoraFlag)
			{
				dir[cont] = g_rxBuffer[0];
				cont++;
			}

			if(0x1B == g_rxBuffer[0])
			{
				escribirMenu(menuString);
			}
		}

		/* If TX is idle and g_txBuffer is full, start to send data. */
		if ((!txOnGoing) && txBufferFull)
		{
			txOnGoing = true;
			UART_TransferSendNonBlocking(UART0, &g_uartHandle, &sendXfer);
		}

		/* If g_txBuffer is empty and g_rxBuffer is full, copy g_rxBuffer to g_txBuffer. */
		if ((!rxBufferEmpty) && (!txBufferFull))
		{
			memcpy(g_txBuffer, g_rxBuffer, 1);
			rxBufferEmpty = true;
			txBufferFull = true;

		}
		/*
		 * Condicionales para ver en que menu se esta.
		 */
		if(((7 == cont) && (true == leerMemoriaFlag)) || ((2 == cont) && (true == longitudB) && (true == leerMemoriaFlag)))
		{
			cont = 0;
			leerMemoria(dir);
		}

		if(((7 == cont) && (true == escribirMemoriaFlag) && (false == longitudB)) || (0xD == g_rxBuffer[0] && true == escribirMemoriaFlag))
		{
			//modificar el tama�o de arreglo que se esta mandando.
			cont = 0;
			escribirMemoria(dir);
		}

		if(true == establecerHoraFlag && 0xD == g_rxBuffer[0])
		{
			cont = 0;
			establecerHora(dir);
		}

		if(true == establecerFechaFlag && 0xD == g_rxBuffer[0])
		{
			cont = 0;
			establecerFecha(dir);
		}

		if(true == formatoHoraFlag && 3 == cont)
		{
			cont = 0;
			formatoHora(dir);
		}
		/*
		 * **********************************
		 */

		if( 0xD == g_rxBuffer[0])
		{
			terminalOpciones();

		}

		if(g_rxBuffer[0] > 48 && g_rxBuffer[0] < 58)
		{
			selector = g_rxBuffer[0];
		}


	}
}

void terminalOpciones()
{
	switch(selector)
	{
	case(49):
		leerMemoriaFlag = true;
		escribirMenu("\033[2J\033[1;1HDireccion de lectura: \r\nLongitud en bytes: \r\nContenido:");
		escribirMenu("\033[1;22H  ");
		terminalCambioMenu();
	break;
	case(50):
		escribirMemoriaFlag = true;
		escribirMenu("\033[2J\033[1;1HDireccion de lectura: \r\nTexto a guardar:");
		escribirMenu("\033[1;22H  ");
		terminalCambioMenu();
	break;
	case(51):
		establecerHoraFlag = true;
		escribirMenu("\033[2J\033[1;1HEscribir hora en hh/mm/ss: ");
		escribirMenu("\033[1;26H  ");
		terminalCambioMenu();
	break;
	case(52):
		establecerFechaFlag = true;
		escribirMenu("\033[2J\033[1;1HEscribir fecha en dd/mm/aa ");
		escribirMenu("\033[1;26H  ");
		terminalCambioMenu();
	break;
	case(53):
		formatoHoraFlag = true;
	//Insertar el formato de hora actual.
		escribirMenu("\033[2J\033[1;1HEl formato actual es \r\nDesea cambiar el formato a 24h"
				     " si/no? ");
		escribirMenu("\033[2;37H  ");
		terminalCambioMenu();
	break;
	case(54):
		leerHora();
	break;
	break;
	case(55):
		leerFecha();
	break;
	}
}

void escribirMenu(uint8_t opcion[])
{
	uint8_t i = 0;
	while(opcion[i]!=0)
	{
		i++;
	}
	transfer.data = opcion;
	transfer.dataSize = i - 1;
	txOnGoing = true;
	UART_TransferSendNonBlocking(UART0, &g_uartHandle, &transfer);
	while (txOnGoing)
	{
	}

	g_rxBuffer[0] = 0;
}

void leerMemoria(uint8_t direccion[] )
{
	//Se ingresan datos a la longitud de bytes
	if(longitudB)
	{


		escribirMenu("\033[4;1H");
		//se escribe el contenido de la memoria
		escribirMenu("\033[5;18HPresiona una tecla para continuar...");
		//las banderas se hacen falsas.
		longitudB = false;
		leerMemoriaFlag = false;
		terminalCambioMenu();
	}
	//Se ingresan datos a la direcci�n de lectura.
	escribirMenu("\033[2;18H  ");
	longitudB = true;
	terminalCambioMenu();
}

void escribirMemoria(uint8_t direccion[] )
{
	//Se ingresan datos a la longitud de bytes
	if(longitudB)
	{

		escribirMenu("\033[4;1H");
		//se escribe el contenido de la memoria
		escribirMenu("\033[5;18HSu texto ha sido guardado...");
		//las banderas se hacen falsas.
		longitudB = false;
		escribirMemoriaFlag = false;
		terminalCambioMenu();
	}
	//Se ingresan los datos a la direcci�n de lectura.
	escribirMenu("\033[3;1H  ");
	longitudB = true;
	terminalCambioMenu();
}

void establecerHora(uint8_t hora[])
{
	//mandar la hora al RTC
	escribirMenu("\033[2;18HLa hora ha sido cambiada...");

}

void establecerFecha(uint8_t fecha[])
{
	//mandar la fecha al RTC.
	escribirMenu("\033[2;18HLa fecha ha sido cambiada...");

}

void formatoHora(uint8_t hora[])
{
	escribirMenu("\033[3;18HEl formato ha sido cambiado...");
}

void leerHora()
{
	//Inserrtar la hora.
	escribirMenu("\033[2J\033[1;1HLa hora actual es:");
}

void leerFecha()
{
	//Inserrtar la fecha.
	escribirMenu("\033[2J\033[1;1HLa fecha actual es:");
}


