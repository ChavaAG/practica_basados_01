/*
 * terminal.h
 *
 *  Created on: 17/03/2017
 *      Author: Alberto
 */

#ifndef SOURCE_TERMINAL_H_
#define SOURCE_TERMINAL_H_

#include "fsl_gpio.h"
#include "board.h"
#include "fsl_uart.h"

#include "pin_mux.h"
#include "clock_config.h"
#include "fsl_debug_console.h"

void terminalInit();
void terminalMenu();
void terminalCambioMenu();
void terminalOpciones();
void escribirMenu(uint8_t opcion[]);
void UART_UserCallback(UART_Type *base, uart_handle_t *handle, status_t status, void *userData);
void leerMemoria(uint8_t direccion[]);
void escribirMemoria(uint8_t direccion[]);
void establecerHora(uint8_t hora[]);
void establecerFecha(uint8_t fecha[]);
void formatoHora(uint8_t hora[]);
void ecoLCD(uint8_t LCD[]);
void leerHora();
void leerFecha();

#endif /* SOURCE_TERMINAL_H_ */
